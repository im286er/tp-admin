<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\common\controller\BaseController;
use app\common\library\token\Token;
use general\Random;
use think\facade\Db;
use app\admin\model\auth\Admin as AdminModel;
use app\admin\model\wechat\Auth as AuthModel;
use app\common\library\wechat\Crawler;

class Index extends BaseController
{

    protected $noNeedLogin = ['*'];
    /**
     * @var AdminModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new AdminModel;
    }

    public function index()
    {
        //echo $this->auth->getToken();
//        list($token, $cookie) = AuthModel::getAuthInfo();
//
//        $crawler = new Crawler();
//
//        $crawler->setToken($token);
//        $crawler->setCookie($cookie);
//
//        $result = $crawler->getAccountAppMsg('MzI2NjQyMjI5Nw==', 0, '芜湖');
//
//        dump($result);
//        dump($crawler->getErrorCode());
//        dump($crawler->getErrorMsg());
        //AuthModel::updateAuth('453684351');
    }

    public function index2()
    {
        $token = Random::uuid();
        //print_r($token);
        //设置信息
        $uid = 1;  //用户uid
        $expire_time = 60*30; //过期时间
        Token::set($token,$uid,$expire_time);
        //获取信息  过期了则为空，没过期可以获取到信息
        $info = Token::get($token);
        print_r($info);
        list($total, $list) = AdminModel::getList();

        var_dump($total);
        var_dump($list->toArray());
        return '您好！这是一个[admin]示例应用';
    }
}
