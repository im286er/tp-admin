<?php
declare (strict_types=1);

namespace app\admin\controller\media;

use app\common\controller\BaseController;
use app\admin\model\media\Group as GroupModel;

class Group extends BaseController
{

    protected $noNeedRight = ['allGroup'];

    /**
     * @var GroupModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new GroupModel;
    }

    public function allGroup()
    {
        $list = $this->model->field('id,name')
            ->where('status', '=', 1)
            ->order(['weigh' => 'desc', 'id' => 'asc'])
            ->select();

        return $this->success('请求成功',$list);
    }

}