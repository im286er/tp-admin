<?php
declare (strict_types = 1);

namespace app\admin\controller\wechat;

use app\common\controller\BaseController;
use app\admin\model\wechat\Auth as AuthModel;

class Auth extends BaseController
{

    /**
     * @var AuthModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new AuthModel;
    }

}