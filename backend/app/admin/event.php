<?php
// 这是系统自动生成的event定义文件
return [
    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => [
            '\app\admin\listener\Log',
        ],
        'LogWrite' => [],
        'RouteLoaded' => [],
        'LogRecord' => [],
        // 更多事件监听
    ],
];
