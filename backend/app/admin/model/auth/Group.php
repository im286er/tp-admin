<?php
declare (strict_types=1);

namespace app\admin\model\auth;

use app\admin\model\BaseModel;

class Group extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'auth_group';

    public function getList($filter = []): array
    {
        $where = function ($query) use ($filter) {
            if (isset($filter['title'])) {
                $query->where('title', 'like', '%' . $filter['title'] . '%');
            }
            if (isset($filter['status'])) {
                $query->where('status', '=', $filter['status']);
            }
        };
        $order = ['id' => 'asc'];
        return self::getPageList($where, $order);
    }

    public function setRulesAttr($value, $data)
    {
        return implode(',', $value);
    }

    public function getRulesAttr($value, $data)
    {
        $rules = explode(',', $value);

        return array_map("intval", $rules);
    }
}