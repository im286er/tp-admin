<?php
declare (strict_types=1);

namespace app\admin\model\auth;

use app\admin\model\BaseModel;

class GroupAccess extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'auth_group_access';
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    // 追加属性
    protected $append = [];
}