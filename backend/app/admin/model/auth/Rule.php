<?php
declare (strict_types=1);

namespace app\admin\model\auth;

use app\admin\model\BaseModel;
use general\Tree;

class Rule extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'auth_rule';

    public function getList($filter = []): array
    {
        $list = $this->order(['weigh' => 'desc', 'id' => 'asc'])->select()->toArray();

        $tree = Tree::instance()->init($list);

        return $tree->getTreeArray();
    }

}