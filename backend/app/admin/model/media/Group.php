<?php
declare (strict_types=1);

namespace app\admin\model\media;

use app\admin\model\BaseModel;

class Group extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'media_group';

    public function getList($filter = []): array
    {
        $where = function ($query) use ($filter) {
            if (isset($filter['name'])) {
                $query->where('name', 'like', '%' . $filter['name'] . '%');
            }
            if (isset($filter['status'])) {
                $query->where('status', '=', $filter['status']);
            }
        };
        $order = ['weigh' => 'desc', 'id' => 'asc'];
        return self::getPageList($where, $order);
    }

}