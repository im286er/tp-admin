<?php
declare (strict_types=1);

namespace app\admin\model\media;

use app\admin\model\BaseModel;

class Website extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'media_website';

    public function getList($filter = []): array
    {
        $where = function ($query) use ($filter) {
            if (isset($filter['group_id'])) {
                $query->where('group_id', '=', $filter['group_id']);
            }
            if (isset($filter['name'])) {
                $query->where('name', 'like', '%' . $filter['name'] . '%');
            }
            if (isset($filter['url'])) {
                $query->where('url', 'like', '%' . $filter['url'] . '%');
            }
            if (isset($filter['status'])) {
                $query->where('status', '=', $filter['status']);
            }
        };
        $order = ['weigh' => 'desc', 'id' => 'asc'];
        return self::getPageList($where, $order);
    }

    public function getPageList($where = [], $order = [], $page = 0, $limit = 0)
    {
        $page = $page ?: request()->param("page", 1, 'intval');
        $limit = $limit ?: request()->param("limit", 10, 'intval');

        $total = self::
        where($where)
            ->count();

        $list = self::with(['group'])
            ->where($where)
            ->order($order)
            ->page($page, $limit)
            ->select();

        return [$total, $list];
    }

    public function changeMoudleGroup($new, $old)
    {
        if ($new != $old) {
            $id = $this->id;
            $group_id = $new;
            $model = new Module();
            $model->where('website_id', '=', $id)->save(['group_id' => $group_id]);
        }
        return true;
    }

    public function changeModuleNum($new, $old = 0)
    {
        //新增
        if ($old == 0) {
            $this->where('id', '=', $new)->inc('module_num', 1)->save();
        }
        //编辑
        if ($new != $old && $old != 0) {
            $this->where('id', '=', $old)->dec('module_num', 1)->save();
            $this->where('id', '=', $new)->inc('module_num', 1)->save();
        }
    }

    public function group()
    {
        return $this->belongsTo('Group', 'group_id', 'id')->field('id,name');
    }

}