<?php

declare (strict_types=1);

namespace app\common\library\storage;

use think\Exception;
use think\facade\Filesystem;
use think\File;
use think\file\UploadedFile;

/**
 * 存储引擎抽象类
 * Class Driver
 * @package app\common\library\storage
 */
abstract class Driver
{
    // 当前存储引擎
    protected $storage;

    // 存储配置
    protected $config;

    // file对象句柄
    /**
     * @var UploadedFile
     */
    protected $file;

    // 验证规则
    protected $validateRuleScene;

    // 磁盘配置
    protected $disk = 'public';

    // 命名规则
    protected $rule;

    // 文件名路径
    protected $hashName;

    // 保存的根文件夹名称
    protected $rootDirName;

    // 错误信息
    protected $error;

    /**
     * 构造函数
     * Server constructor.
     * @param string $storage 存储方式
     * @param array|null $config 存储配置
     */
    public function __construct(string $storage, array $config = null)
    {
        $this->storage = $storage;
        $this->config = $config;
    }

    /**
     * 设置上传的文件信息 (外部用户上传)
     * @param $file File
     * @return $this
     */
    public function setUploadFile($file)
    {
        // 接收上传的文件
        try {
            $this->file = $file;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }
        if (empty($this->file)) {
            $this->error = '未找到上传文件的信息';
        }
        return $this;
    }

    /**
     * 设置上传的文件信息 (系统内部上传)
     * @param string $filePath 文件路径
     * @return $this
     */
    public function setUploadFileByReal(string $filePath)
    {
        // 接收上传的文件
        $this->file = new UploadedFile($filePath, basename($filePath));
        if (empty($this->file)) {
            $this->error = '未找到上传文件的信息';
        }
        return $this;
    }

    /**
     * 设置上传文件的验证规则
     * @param string $scene
     * @return mixed
     */
    public function setValidationScene(string $scene = '')
    {
        $this->validateRuleScene = $scene;
        return $this;
    }

    /**
     * 设置磁盘配置
     * @param string $disk
     * @return $this
     */
    public function setDisk(string $disk)
    {
        $this->disk = $disk;
        $this->config = Filesystem::getDiskConfig($disk);
        return $this;
    }

    /**
     * 设置文件命名规则
     * @param string $rule
     * @return $this
     */
    public function setRule(string $rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * 文件名路径
     * @param string $rule
     * @return $this
     */
    public function setHashName(string $hashName)
    {
        $this->hashName = $hashName;
        return $this;
    }

    /**
     * 设置上传的文件根目录名称
     * @param string $name
     * @return $this
     */
    public function setRootDirName(string $name)
    {
        $this->rootDirName = $name;
        return $this;
    }

    /**
     * 文件上传
     * @return mixed
     */
    abstract function upload();

    /**
     * 文件删除
     * @param string $filePath
     * @return mixed
     */
    abstract function delete(string $filePath);

    /**
     * 临时文件的绝对路径
     * @return mixed
     */
    protected function getRealPath(): string
    {
        return $this->file->getRealPath();
    }

    /**
     * 返回错误信息
     * @return mixed
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * 生成保存的文件信息
     * @return array
     */
    public function getSaveFileInfo(): array
    {
        // 自动生成的文件名称
        $hashName = $this->getHashName();
        // 存储目录
        $filePath = $this->getFilePath($hashName);
        // 文件名称
        // 去除扩展名的写法 stristr($this->file->getOriginalName(), '.', true)
        $fileName = $this->file->getOriginalName();
        // 文件扩展名
        $fileExt = strtolower($this->file->extension());
        // 存储域名
        $domain = $this->config['url'] ?? '';
        //访问路径
        $fileUrl = $this->getFileUrl();
        return [
            'storage' => $this->storage,                 // 存储方式
            'domain' => $domain,                         // 存储域名
            'file_url' => $fileUrl,                      // 访问路径
            'file_path' => $filePath,                    // 文件路径
            'file_name' => $fileName,                    // 文件名称
            'file_size' => $this->file->getSize(),       // 文件大小(字节)
            'file_ext' => $fileExt,                      // 文件扩展名
            'file_type' => $this->file->getMime(),       // 文件类型
            'file_sha1' => $this->file->sha1(),          // 文件SHA1值
        ];
    }

    public function getFileUrl()
    {
        $hashName = $this->getHashName();
        $path = $this->getFilePath($hashName);

        if (strpos($path, '/') === 0) {
            return $path;
        }
        if (isset($this->config['url']) && $this->config['url']) {
            $url = $this->config['url'] . DIRECTORY_SEPARATOR . $path;
        } else {
            $url = $path;
        }
        $url = str_replace('\\', '/', $url);
        return $url;
    }

    /**
     * 生成文件名
     * @return string
     */
    private function getHashName()
    {
        if (!$this->hashName) {
            return $this->file->hashName($this->rule);
        } else {
            return $this->hashName;
        }
    }

    /**
     * 获取hashName的路径
     * @param string $hashName
     * @return string
     */
    private function getFilePath(string $hashName): string
    {
        $filePath = empty($this->rootDirName) ? "{$hashName}" : "{$this->rootDirName}/{$hashName}";
        $filePath = str_replace('\\', '/', $filePath);
        return $filePath;
    }

    /**
     * 获取hashName的文件名
     * @param string $filePath
     * @return string
     */
    protected function getFileHashName(string $filePath): string
    {
        return basename($filePath);
    }

    /**
     * 获取hashName的文件目录
     * @param string $filePath
     * @return string
     */
    protected function getFileHashRoute(string $filePath): string
    {
        return dirname($filePath);
    }

}
