<?php

declare (strict_types=1);

namespace app\common\library\storage\engine;

use app\common\library\storage\Driver;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Qiniu\Config;
use Qiniu\Storage\BucketManager;

/**
 * 七牛云存储引擎
 * Class Qiniu
 * @package app\common\library\storage\engine
 */
class Qiniu extends Driver
{
    /**
     * 执行上传
     * @return bool|mixed
     * @throws \Exception
     */
    public function upload()
    {
        // 要上传图片的本地路径
        $realPath = $this->getRealPath();
        // 构建鉴权对象
        $auth = new Auth($this->config['accessKey'], $this->config['secretKey']);
        // 要上传的空间
        $token = $auth->uploadToken($this->config['bucket']);
        // 初始化
        $uploadMgr = new UploadManager();
        // 调用 UploadManager 的 putFile 方法进行文件的上传
        list($ret, $error) = $uploadMgr->putFile($token, $this->getSaveFileInfo()['file_path'], $realPath);
        if ($error !== null) {
            //$this->error = $error->message();
            $this->error = '上传失败';
            return false;
        }
        return true;
    }

    /**
     * 删除文件
     * @param string $filePath
     * @return bool|mixed
     */
    public function delete(string $filePath)
    {
        // 构建鉴权对象
        $auth = new Auth($this->config['accessKey'], $this->config['secretKey']);
        // 初始化
        $config = new \Qiniu\Config();
        $bucketMgr = new BucketManager($auth, $config);
        $error = $bucketMgr->delete($this->config['bucket'], $filePath);
        if ($error[1] !== null) {
            //$this->error = $error->message();
            $this->error = '文件不存在';
            return false;
        }
        return true;
    }

}
