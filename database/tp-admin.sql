/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : gitee-tp-admin

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2022-06-20 22:20:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_admin
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin`;
CREATE TABLE `tp_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登录名',
  `realname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `login_failure` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `login_time` int(11) DEFAULT NULL COMMENT '登录时间',
  `login_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录IP',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登陆标识',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of tp_admin
-- ----------------------------
INSERT INTO `tp_admin` VALUES ('1', 'admin', '超级管理员', '63d6e978e0c03145820f3f0d76682b6f', 'TVJBeL', 'https://qiniu-cdn.yinhoujie.com/image/13/bdb2f887e7001267b53150e2ddfbc3.jpeg', '123456789@qq.com', '0', '1655734759', '127.0.0.1', '1', '', '1551170841', '1655734759');

-- ----------------------------
-- Table structure for tp_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin_log`;
CREATE TABLE `tp_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '请求地址',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型',
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'IP',
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '浏览器',
  `createtime` int(11) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=986 DEFAULT CHARSET=utf8mb4 COMMENT='管理员日志表';

-- ----------------------------
-- Records of tp_admin_log
-- ----------------------------
INSERT INTO `tp_admin_log` VALUES ('985', '1', 'admin', '/admin/login/login', '登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36', '1655734759');

-- ----------------------------
-- Table structure for tp_admin_token
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin_token`;
CREATE TABLE `tp_admin_token` (
  `token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `expiretime` int(11) DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员Token表';

-- ----------------------------
-- Records of tp_admin_token
-- ----------------------------
INSERT INTO `tp_admin_token` VALUES ('bc57e18708858da035615dfa5e7823a38d15f134', '1', '1655734759', '1658326759');

-- ----------------------------
-- Table structure for tp_attachment
-- ----------------------------
DROP TABLE IF EXISTS `tp_attachment`;
CREATE TABLE `tp_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '访问路径',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `storage` varchar(100) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `domain` varchar(100) NOT NULL DEFAULT '' COMMENT '存储域名',
  `filename` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名称',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `suffix` varchar(20) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) NOT NULL DEFAULT '' COMMENT '透传数据',
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件sha1编码',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建日期',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for tp_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `tp_auth_group`;
CREATE TABLE `tp_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '组名',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `rules` text NOT NULL COMMENT '规则ID',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='分组表';

-- ----------------------------
-- Records of tp_auth_group
-- ----------------------------
INSERT INTO `tp_auth_group` VALUES ('1', '0', '超级管理员', '', '*', '1', '1551170841', null);
INSERT INTO `tp_auth_group` VALUES ('2', '0', '普通管理员', '管理员', '4,8,40,44,17,48,85,52,53,26,57,64,65,69,72,76,77,81,82,38,39,32,19,1,37,3,9,2,10,11,12,84,25,24,27,34,33,35,36', '1', '1651654703', '1655698577');
INSERT INTO `tp_auth_group` VALUES ('4', '0', '其他管理员', '', '4,1,8,12,48,49,2', '1', '1651657605', '1655606668');
INSERT INTO `tp_auth_group` VALUES ('5', '0', '测试管理员', '', '40,41,42,44,10,45,46,47,17,21,22,48,52,25,53,54,55,56,26,9,2,12,24,11', '1', '1651762680', '1655606648');

-- ----------------------------
-- Table structure for tp_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `tp_auth_group_access`;
CREATE TABLE `tp_auth_group_access` (
  `uid` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限分组表';

-- ----------------------------
-- Records of tp_auth_group_access
-- ----------------------------
INSERT INTO `tp_auth_group_access` VALUES ('1', '1');

-- ----------------------------
-- Table structure for tp_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `tp_auth_rule`;
CREATE TABLE `tp_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则标题',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称:后端权限',
  `permission` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称:前端权限',
  `path` varchar(255) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT '' COMMENT '组件/页面路径',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '注：没啥用，可以删除',
  `condition` varchar(100) NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `keepalive` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否缓存',
  `show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `weigh` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COMMENT='节点表';

-- ----------------------------
-- Records of tp_auth_rule
-- ----------------------------
INSERT INTO `tp_auth_rule` VALUES ('1', '0', 'Dashboard', 'dashboard', '', '/dashboard', 'LAYOUT', '', '1', '', '', '1', '0', '1', '100', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('2', '0', '系统管理', 'auth', '', '/auth', 'LAYOUT', '', '1', '', '', '1', '0', '1', '90', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('3', '0', '关于', 'about', '', '/about', 'LAYOUT', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', '1652190964');
INSERT INTO `tp_auth_rule` VALUES ('4', '1', '分析台', 'analysis', '', 'analysis', '/dashboard/analysis/index', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', '1653494036');
INSERT INTO `tp_auth_rule` VALUES ('8', '1', '工作台', 'workbench', '', 'workbench', '/dashboard/workbench/index', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('9', '2', '账号管理', 'admin', '', 'admin', '/auth/admin/index', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('10', '2', '角色管理', 'group', '', 'group', '/auth/group/index', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('11', '2', '菜单管理', 'rule', '', 'rule', '/auth/rule/index', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('12', '2', '操作日志', 'log', '', 'log', '/auth/log/index', '', '1', '', '', '1', '0', '1', '0', '1', '1551170841', null);
INSERT INTO `tp_auth_rule` VALUES ('17', '11', '菜单列表', 'auth:rule:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1651594206', '1651594206');
INSERT INTO `tp_auth_rule` VALUES ('19', '3', 'hao123', 'hao123', '', 'http://www.hao123.com', 'IFrame', '', '1', '', '', '1', '0', '1', '0', '1', '1651653362', '1651653368');
INSERT INTO `tp_auth_rule` VALUES ('20', '11', '菜单添加', 'auth:rule:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1651940058', '1651940058');
INSERT INTO `tp_auth_rule` VALUES ('21', '11', '菜单编辑', 'auth:rule:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1651940083', '1651940083');
INSERT INTO `tp_auth_rule` VALUES ('22', '11', '菜单删除', 'auth:rule:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1651940115', '1651940115');
INSERT INTO `tp_auth_rule` VALUES ('24', '0', '微信管理', 'wechat', '', '/wechat', 'LAYOUT', '', '1', '', '', '1', '0', '1', '80', '1', '1652882194', '1652882194');
INSERT INTO `tp_auth_rule` VALUES ('25', '24', '公众号管理', 'account', '', 'account', '/wechat/account/index', '', '1', '', '', '1', '0', '1', '0', '1', '1652882720', '1652882720');
INSERT INTO `tp_auth_rule` VALUES ('26', '24', '文章列表', 'accountmsg', '', 'article/:nickname/:fakeid', '/wechat/article/list', '', '1', '', '', '1', '0', '0', '0', '1', '1653223458', '1653317533');
INSERT INTO `tp_auth_rule` VALUES ('27', '24', '公众号文章', 'accountarticle', '', 'article', '/wechat/article/index', '', '1', '', '', '1', '0', '1', '0', '1', '1653317603', '1653317603');
INSERT INTO `tp_auth_rule` VALUES ('28', '2', '消息队列', 'queue', '', '/general/queue', '/general/queue/log', '', '1', '', '', '1', '0', '1', '0', '1', '1653398265', '1653402056');
INSERT INTO `tp_auth_rule` VALUES ('29', '24', 'cookie管理', 'wechatcookie', '', 'auth', '/wechat/auth/index', '', '1', '', '', '1', '0', '1', '0', '1', '1653400096', '1653400096');
INSERT INTO `tp_auth_rule` VALUES ('30', '0', '个人中心', 'profile', '', '/profile', 'LAYOUT', '', '1', '', '', '1', '0', '1', '10', '1', '1653658673', '1653662963');
INSERT INTO `tp_auth_rule` VALUES ('31', '30', '修改密码', 'general:profile:changepwd', '', 'password', '/general/password/index', '', '1', '', '', '1', '0', '1', '0', '1', '1653658743', '1655608710');
INSERT INTO `tp_auth_rule` VALUES ('32', '3', '百度一下', 'baidu', '', 'https://www.baidu.com/', 'IFrame', '', '1', '', '', '1', '0', '1', '10', '1', '1653663003', '1653663003');
INSERT INTO `tp_auth_rule` VALUES ('33', '0', '信息采集', 'media', '', '/media', 'LAYOUT', '', '1', '', '', '1', '0', '1', '70', '1', '1653919409', '1653919439');
INSERT INTO `tp_auth_rule` VALUES ('34', '33', '分组管理', 'media:group', '', 'group', '/media/group/index', '', '1', '', '', '1', '0', '1', '100', '1', '1653919614', '1653919614');
INSERT INTO `tp_auth_rule` VALUES ('35', '33', '网站管理', 'media:website', '', 'website', '/media/website/index', '', '1', '', '', '1', '0', '1', '90', '1', '1653921591', '1653921599');
INSERT INTO `tp_auth_rule` VALUES ('36', '33', '模块管理', 'media:module', '', 'module', '/media/module/index', '', '1', '', '', '1', '0', '1', '80', '1', '1654008831', '1654008906');
INSERT INTO `tp_auth_rule` VALUES ('37', '33', '采集文章', 'media:article', '', 'article', '/media/article/index', '', '1', '', '', '1', '0', '1', '0', '1', '1654265415', '1654265415');
INSERT INTO `tp_auth_rule` VALUES ('38', '33', '模块文章', 'module:article', '', 'article/module/:id', '/media/article/listModule', '', '1', '', '', '1', '0', '0', '0', '1', '1654696800', '1654871438');
INSERT INTO `tp_auth_rule` VALUES ('39', '33', '网站文章', 'website:article', '', 'article/website/:id', '/media/article/listWebsite', '', '1', '', '', '1', '0', '0', '0', '1', '1654870797', '1654871449');
INSERT INTO `tp_auth_rule` VALUES ('40', '9', '账号列表', 'auth:admin:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304465', '1655304465');
INSERT INTO `tp_auth_rule` VALUES ('41', '9', '账号添加', 'auth:admin:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('42', '9', '账号编辑', 'auth:admin:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('43', '9', '账号删除', 'auth:admin:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('44', '10', '角色列表', 'auth:group:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('45', '10', '角色添加', 'auth:group:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('46', '10', '角色编辑', 'auth:group:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('47', '10', '角色删除', 'auth:group:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('48', '12', '日志列表', 'auth:log:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('49', '12', '日志删除', 'auth:log:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('50', '28', '队列列表', 'general:queuelog:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('51', '28', '队列删除', 'general:queuelog:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('52', '25', '公众号列表', 'wechat:account:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('53', '25', '公众号详情', 'wechat:account:read', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('54', '25', '公众号添加', 'wechat:account:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('55', '25', '公众号编辑', 'wechat:account:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('56', '25', '公众号删除', 'wechat:account:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('57', '27', '文章列表', 'wechat:article:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('58', '27', '文章同步', 'wechat:article:syncarticle', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('59', '27', '文章下载', 'wechat:article:download', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('60', '29', 'cookie列表', 'wechat:auth:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('61', '29', 'cookie添加', 'wechat:auth:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('62', '29', 'cookie编辑', 'wechat:auth:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('63', '29', 'cookie删除', 'wechat:auth:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('64', '34', '分组列表', 'media:group:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('65', '34', '分组详情', 'media:group:read', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('66', '34', '分组添加', 'media:group:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('67', '34', '分组编辑', 'media:group:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('68', '34', '分组删除', 'media:group:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('69', '35', '网站列表', 'media:website:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('72', '35', '网站详情', 'media:website:read', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('73', '35', '网站添加', 'media:website:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('74', '35', '网站编辑', 'media:website:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('75', '35', '网站删除', 'media:website:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('76', '36', '模块列表', 'media:module:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('77', '36', '模块详情', 'media:module:read', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('78', '36', '模块添加', 'media:module:add', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('79', '36', '模块编辑', 'media:module:edit', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('80', '36', '模块删除', 'media:module:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('81', '37', '文章列表', 'media:article:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('82', '37', '文章同步', 'media:article:syncarticle', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655304487', '1655304487');
INSERT INTO `tp_auth_rule` VALUES ('84', '2', '附件管理', 'attachment', '', 'attachment', '/general/attachment/index', '', '1', '', '', '1', '0', '1', '0', '1', '1655541252', '1655541252');
INSERT INTO `tp_auth_rule` VALUES ('85', '84', '附件列表', 'general:attachment:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655541252', '1655541252');
INSERT INTO `tp_auth_rule` VALUES ('86', '84', '附件删除', 'general:attachment:del', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655541252', '1655541252');
INSERT INTO `tp_auth_rule` VALUES ('87', '0', '其他', 'ajax', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', null, null);
INSERT INTO `tp_auth_rule` VALUES ('88', '87', '文件上传', 'upload:index', '', '', '', '', '1', '', '', '0', '0', '1', '0', '1', '1655608459', '1655608509');

-- ----------------------------
-- Table structure for tp_media_article
-- ----------------------------
DROP TABLE IF EXISTS `tp_media_article`;
CREATE TABLE `tp_media_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '文章唯一标识',
  `module_id` int(11) NOT NULL COMMENT '模块名称',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '文章标题',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '封面图',
  `url` varchar(1000) NOT NULL DEFAULT '' COMMENT '链接',
  `time` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '额外的信息',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `code` (`code`) USING BTREE,
  KEY `module_id` (`module_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COMMENT='采集的文章';

-- ----------------------------
-- Records of tp_media_article
-- ----------------------------

-- ----------------------------
-- Table structure for tp_media_group
-- ----------------------------
DROP TABLE IF EXISTS `tp_media_group`;
CREATE TABLE `tp_media_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '分组名称',
  `intro` varchar(255) NOT NULL DEFAULT '' COMMENT '分组简介',
  `weigh` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='采集分组';

-- ----------------------------
-- Records of tp_media_group
-- ----------------------------

-- ----------------------------
-- Table structure for tp_media_module
-- ----------------------------
DROP TABLE IF EXISTS `tp_media_module`;
CREATE TABLE `tp_media_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(11) NOT NULL COMMENT '分组ID',
  `website_id` int(11) NOT NULL COMMENT '网站ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '模块名称',
  `url` varchar(500) NOT NULL DEFAULT '' COMMENT '链接',
  `intro` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `article_num` int(11) NOT NULL DEFAULT '0' COMMENT '文章数量',
  `times` int(11) NOT NULL DEFAULT '60' COMMENT '采集频率（秒）',
  `rules` text NOT NULL COMMENT '采集规则',
  `error_count` int(11) NOT NULL DEFAULT '0' COMMENT '错误次数',
  `error_msg` varchar(255) NOT NULL DEFAULT '' COMMENT '错误信息',
  `weigh` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='采集模块';

-- ----------------------------
-- Records of tp_media_module
-- ----------------------------

-- ----------------------------
-- Table structure for tp_media_website
-- ----------------------------
DROP TABLE IF EXISTS `tp_media_website`;
CREATE TABLE `tp_media_website` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(11) NOT NULL COMMENT '分组ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '网站名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '网址',
  `logo` varchar(255) NOT NULL DEFAULT '' COMMENT 'LOGO',
  `intro` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `module_num` int(11) NOT NULL DEFAULT '0' COMMENT '模块数量',
  `article_num` int(11) NOT NULL DEFAULT '0' COMMENT '文章数量',
  `weigh` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='采集网站';

-- ----------------------------
-- Records of tp_media_website
-- ----------------------------

-- ----------------------------
-- Table structure for tp_queue_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_queue_log`;
CREATE TABLE `tp_queue_log` (
  `id` varchar(50) NOT NULL COMMENT 'redis返回的key，队列标识',
  `queue` varchar(255) NOT NULL DEFAULT '' COMMENT '队列名称',
  `class` varchar(255) NOT NULL DEFAULT '' COMMENT '队列执行类',
  `payload` longtext NOT NULL COMMENT '队列传输数据',
  `attempts` tinyint(4) NOT NULL DEFAULT '0' COMMENT '任务重试次数',
  `status` tinyint(4) NOT NULL DEFAULT '10' COMMENT '任务状态:10=待执行,20=执行成功,30=执行失败,40=已取消',
  `err_msg` varchar(255) NOT NULL DEFAULT '' COMMENT '错误信息',
  `execution_time` int(11) DEFAULT NULL COMMENT '任务执行时间',
  `createtime` int(11) DEFAULT NULL COMMENT '任务创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '任务更新时间',
  `deletetime` int(11) DEFAULT NULL COMMENT '任务删除时间（删除后队列判断状态不再执行）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_queue_log
-- ----------------------------

-- ----------------------------
-- Table structure for tp_wechat_account
-- ----------------------------
DROP TABLE IF EXISTS `tp_wechat_account`;
CREATE TABLE `tp_wechat_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT '公众号APPID',
  `nickname` varchar(20) NOT NULL DEFAULT '' COMMENT '公众号名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '公众号头像',
  `round_head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '公众号头像（圆形）',
  `alias` varchar(50) NOT NULL DEFAULT '' COMMENT '公众号的微信号',
  `fakeid` varchar(50) NOT NULL DEFAULT '' COMMENT '公众号唯一标识',
  `service_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '公众号类型:0,1=订阅号,2=服务号',
  `signature` varchar(255) NOT NULL DEFAULT '' COMMENT '公众号描述',
  `qrcode` varchar(255) NOT NULL DEFAULT '' COMMENT '公众号二维码',
  `msg_count` int(11) NOT NULL DEFAULT '0' COMMENT '已发布的文章数量',
  `weigh` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `deletetime` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='微信公众号';

-- ----------------------------
-- Records of tp_wechat_account
-- ----------------------------

-- ----------------------------
-- Table structure for tp_wechat_article
-- ----------------------------
DROP TABLE IF EXISTS `tp_wechat_article`;
CREATE TABLE `tp_wechat_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `a_appid` varchar(50) NOT NULL DEFAULT '' COMMENT '公众号APPID',
  `a_fakeid` varchar(50) NOT NULL DEFAULT '' COMMENT '公众号唯一标识',
  `a_nickname` varchar(20) NOT NULL DEFAULT '' COMMENT '公众号名称',
  `aid` varchar(20) NOT NULL DEFAULT '' COMMENT '公众号文章ID',
  `appmsgid` varchar(20) NOT NULL DEFAULT '' COMMENT '公众号文章消息ID',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '文章标题',
  `digest` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `itemidx` tinyint(1) NOT NULL DEFAULT '0' COMMENT '显示位置',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '封面图',
  `link` varchar(255) NOT NULL DEFAULT '' COMMENT '文章链接',
  `copyright_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '原创:1=是,0=否',
  `auhtor` varchar(20) DEFAULT '' COMMENT '作者',
  `content` text COMMENT '文章内容',
  `html` varchar(255) DEFAULT '' COMMENT '保存的html文件',
  `word` varchar(255) DEFAULT '' COMMENT '保存的word文件',
  `pdf` varchar(255) DEFAULT '' COMMENT '保存的pdf文件',
  `createtime` int(11) DEFAULT NULL COMMENT '文章发布时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '文章更新时间',
  PRIMARY KEY (`id`),
  KEY `a_appid` (`a_appid`) USING BTREE,
  KEY `a_fakeid` (`a_fakeid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3711 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_wechat_article
-- ----------------------------

-- ----------------------------
-- Table structure for tp_wechat_auth
-- ----------------------------
DROP TABLE IF EXISTS `tp_wechat_auth`;
CREATE TABLE `tp_wechat_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `account` varchar(20) NOT NULL DEFAULT '' COMMENT '公众号名称',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT '微信登录token',
  `cookie` text NOT NULL COMMENT '微信登录cookie',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=禁用',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_wechat_auth
-- ----------------------------
