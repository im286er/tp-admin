import { defHttp } from '/@/utils/http/axios';

enum Api {
  QueueLogList = '/general.queueLog/index',
  QueueLogDetail = '/general.queueLog/read',
  QueueLogAdd = '/general.queueLog/add',
  QueueLogEdit = '/general.queueLog/edit',
  QueueLogDel = '/general.queueLog/del',

  AttachmentList = '/general.attachment/index',
  AttachmentDel = '/general.attachment/del',

  ChangePwd = '/general.profile/changepwd',
}

export const getQueueLogList = (params?: any) =>
  defHttp.get<any>({ url: Api.QueueLogList, params });

export const QueueLogDetail = (params?: any) =>
  defHttp.get<any>({ url: Api.QueueLogDetail, params });

export const QueueLogAdd = (params?: any) => defHttp.post<any[]>({ url: Api.QueueLogAdd, params });

export const QueueLogEdit = (params?: any) => defHttp.put<any[]>({ url: Api.QueueLogEdit, params });

export const QueueLogDel = (params?: any) =>
  defHttp.delete<any[]>({ url: Api.QueueLogDel, params });

export const getAttachmentList = (params?: any) =>
  defHttp.get<any>({ url: Api.AttachmentList, params });

export const AttachmentDel = (params?: any) =>
  defHttp.delete<any[]>({ url: Api.AttachmentDel, params });

export const ChangePwd = (params?: any) => defHttp.post<any[]>({ url: Api.ChangePwd, params });
