import { defHttp } from '/@/utils/http/axios';

enum Api {
  GroupList = '/media.group/index',
  GroupDetail = '/media.group/read',
  GroupAdd = '/media.group/add',
  GroupEdit = '/media.group/edit',
  GroupDel = '/media.group/del',
  AllGroupList = '/media.group/allGroup',
  WebsiteList = '/media.website/index',
  WebsiteDetail = '/media.website/read',
  WebsiteAdd = '/media.website/add',
  WebsiteEdit = '/media.website/edit',
  WebsiteDel = '/media.website/del',
  AllWebsiteList = '/media.website/allWebsite',
  ModuleList = '/media.module/index',
  ModuleDetail = '/media.module/read',
  ModuleAdd = '/media.module/add',
  ModuleEdit = '/media.module/edit',
  ModuleDel = '/media.module/del',
  AllModuleList = '/media.website/allModule',
  DefaultRule = '/media.module/defaultRule',
  ArticleList = '/media.article/index',
  syncArticle = '/media.article/syncArticle',
}

export const getGroupList = (params?: any) => defHttp.get<any>({ url: Api.GroupList, params });

export const GroupDetail = (params?: any) => defHttp.get<any>({ url: Api.GroupDetail, params });

export const GroupAdd = (params?: any) => defHttp.post<any[]>({ url: Api.GroupAdd, params });

export const GroupEdit = (params?: any) => defHttp.put<any[]>({ url: Api.GroupEdit, params });

export const GroupDel = (params?: any) => defHttp.delete<any[]>({ url: Api.GroupDel, params });

export const AllGroupList = (params?: any) => defHttp.get<any>({ url: Api.AllGroupList, params });

export const getWebsiteList = (params?: any) => defHttp.get<any>({ url: Api.WebsiteList, params });

export const WebsiteDetail = (params?: any) => defHttp.get<any>({ url: Api.WebsiteDetail, params });

export const WebsiteAdd = (params?: any) => defHttp.post<any[]>({ url: Api.WebsiteAdd, params });

export const WebsiteEdit = (params?: any) => defHttp.put<any[]>({ url: Api.WebsiteEdit, params });

export const WebsiteDel = (params?: any) => defHttp.delete<any[]>({ url: Api.WebsiteDel, params });

export const AllWebsiteList = (params?: any) =>
  defHttp.get<any>({ url: Api.AllWebsiteList, params });

export const getModuleList = (params?: any) => defHttp.get<any>({ url: Api.ModuleList, params });

export const ModuleDetail = (params?: any) => defHttp.get<any>({ url: Api.ModuleDetail, params });

export const ModuleAdd = (params?: any) => defHttp.post<any[]>({ url: Api.ModuleAdd, params });

export const ModuleEdit = (params?: any) => defHttp.put<any[]>({ url: Api.ModuleEdit, params });

export const ModuleDel = (params?: any) => defHttp.delete<any[]>({ url: Api.ModuleDel, params });

export const AllModuleList = (params?: any) => defHttp.get<any>({ url: Api.AllModuleList, params });

export const DefaultRule = (params?: any) => defHttp.get<any>({ url: Api.DefaultRule, params });

export const getArticleList = (params?: any) => defHttp.get<any>({ url: Api.ArticleList, params });

export const syncArticle = (params?: any) => defHttp.post<any[]>({ url: Api.syncArticle, params });
