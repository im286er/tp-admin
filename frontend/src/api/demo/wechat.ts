import { defHttp } from '/@/utils/http/axios';

enum Api {
  AccountList = '/wechat.account/index',
  AccountDetail = '/wechat.account/read',
  AccountAdd = '/wechat.account/add',
  AccountEdit = '/wechat.account/edit',
  AccountDel = '/wechat.account/del',
  AjaxAccount = '/wechat.account/getAccountList',
  ArticleList = '/wechat.article/index',
  AuthList = '/wechat.auth/index',
  AuthAdd = '/wechat.auth/add',
  AuthEdit = '/wechat.auth/edit',
  AuthDel = '/wechat.auth/del',
  syncArticle = '/wechat.article/syncArticle',
  downloadArticle = '/wechat.article/download',
}

export const getAccountList = (params?: any) => defHttp.get<any>({ url: Api.AccountList, params });

export const AccountDetail = (params?: any) => defHttp.get<any>({ url: Api.AccountDetail, params });

export const AccountAdd = (params?: any) => defHttp.post<any[]>({ url: Api.AccountAdd, params });

export const AccountEdit = (params?: any) => defHttp.put<any[]>({ url: Api.AccountEdit, params });

export const AccountDel = (params?: any) => defHttp.delete<any[]>({ url: Api.AccountDel, params });

export const ajaxAccountList = (params?: any) => defHttp.get<any>({ url: Api.AjaxAccount, params });

export const getArticleList = (params?: any) => defHttp.get<any>({ url: Api.ArticleList, params });

export const getAuthList = (params?: any) => defHttp.get<any>({ url: Api.AuthList, params });

export const AuthAdd = (params?: any) => defHttp.post<any[]>({ url: Api.AuthAdd, params });

export const AuthEdit = (params?: any) => defHttp.put<any[]>({ url: Api.AuthEdit, params });

export const AuthDel = (params?: any) => defHttp.delete<any[]>({ url: Api.AuthDel, params });

export const syncArticle = (params?: any) => defHttp.post<any[]>({ url: Api.syncArticle, params });

export const downloadArticle = (params?: any) =>
  defHttp.get<any>({ url: Api.downloadArticle, params });
