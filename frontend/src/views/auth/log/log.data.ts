import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
// import { JsonPreview } from '/@/components/CodeEditor';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
  },
  {
    title: '管理员',
    dataIndex: 'username',
    width: 100,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 200,
  },
  {
    title: '请求地址',
    dataIndex: 'url',
  },
  {
    title: '请求类型',
    dataIndex: 'method',
    width: 100,
    customRender: ({ record }) => {
      const method = record.method;
      const color = {
        GET: 'success',
        POST: 'processing',
        PUT: 'warning',
        DELETE: 'error',
      };

      return h(Tag, { color: color[method] }, () => method);
    },
  },
  {
    title: 'IP',
    dataIndex: 'ip',
    width: 150,
  },
  {
    title: '创建时间',
    dataIndex: 'createtime',
    width: 200,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'admin_id',
    label: '管理员',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'title',
    label: '操作',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'username',
    label: '管理员',
    component: 'Input',
  },
  {
    field: 'title',
    label: '标题',
    component: 'Input',
  },
  {
    field: 'url',
    label: '请求地址',
    component: 'Input',
  },
  {
    field: 'method',
    label: '请求类型',
    component: 'Input',
  },
  {
    field: 'ip',
    label: 'IP',
    component: 'Input',
  },
  {
    field: 'createtime',
    label: '时间',
    component: 'Input',
  },
  {
    field: 'user_agent',
    label: '用户代理',
    component: 'Input',
  },
  {
    field: 'content',
    label: '请求参数',
    component: 'InputTextArea',
    componentProps: {
      rows: 5,
    },
    slot: 'json',
    // render: ({ model, field }) => {
    //   return h(JsonPreview, { data: model[field] });
    // },
  },
];
