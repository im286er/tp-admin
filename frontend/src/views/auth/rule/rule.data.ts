import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { Icon } from '/@/components/Icon';

export const columns: BasicColumn[] = [
  {
    title: '菜单名称',
    dataIndex: 'title',
    width: 200,
    align: 'left',
  },
  {
    title: '类型',
    dataIndex: 'ismenu',
    width: 80,
    customRender: ({ record }) => {
      const status = record.ismenu;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '菜单' : '数据';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '图标',
    dataIndex: 'icon',
    width: 50,
    customRender: ({ record }) => {
      return h(Icon, { icon: record.icon });
    },
  },
  // {
  //   title: '权限标识',
  //   dataIndex: 'permission',
  //   width: 200,
  // },
  {
    title: '权限标识',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '路由',
    dataIndex: 'path',
    width: 200,
  },
  {
    title: '组件',
    dataIndex: 'component',
    width: 300,
  },
  {
    title: '排序',
    dataIndex: 'weigh',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createtime',
    width: 180,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'title',
    label: '菜单名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '停用', value: 0 },
      ],
    },
    colProps: { span: 8 },
  },
];

const isMenu = (ismenu: number) => ismenu === 1;

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'InputNumber',
    defaultValue: 0,
    show: false,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
  {
    field: 'ismenu',
    label: '菜单类型',
    helpMessage: [
      '菜单：左侧显示的菜单，例如：用户管理，员工管理',
      '数据：页面请求后台的接口，例如：添加，编辑，删除',
    ],
    component: 'RadioGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '菜单', value: 1 },
        { label: '数据', value: 0 },
      ],
    },
    //colProps: { lg: 24, md: 24 },
  },
  {
    field: 'pid',
    label: '上级菜单',
    component: 'TreeSelect',
    defaultValue: 0,
    componentProps: {
      replaceFields: {
        title: 'title',
        key: 'id',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
  },
  {
    field: 'title',
    label: '菜单名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'name',
    label: '权限标识',
    helpMessage: ['前后端进行权限控制时的唯一标识'],
    component: 'Input',
    required: true,
  },
  {
    field: 'path',
    label: '路由地址',
    helpMessage: [
      '网址 # 号后的路径，例如：/dashboard/workbench',
      '外链直接输入网址，例如：https://www.baidu.com',
    ],
    component: 'Input',
    required: true,
    ifShow: ({ values }) => isMenu(values.ismenu),
  },
  {
    field: 'component',
    label: '组件路径',
    helpMessage: ['一级菜单：LAYOUT,IFrame', '二级菜单：前端项目src/views后的页面路径'],
    component: 'Input',
    required: true,
    ifShow: ({ values }) => isMenu(values.ismenu),
  },
  {
    field: 'icon',
    label: '图标',
    component: 'IconPicker',
    componentProps: {
      readonly: true,
      placeholder: '请点击右侧选择图标',
    },
    ifShow: ({ values }) => isMenu(values.ismenu),
  },
  // {
  //   field: 'permission',
  //   label: '权限标识',
  //   helpMessage: '前端对按钮进行权限控制时的唯一标识',
  //   component: 'Input',
  //   required: true,
  //   ifShow: ({ values }) => !isMenu(values.ismenu),
  // },
  {
    field: 'keepalive',
    label: '是否缓存',
    component: 'Switch',
    defaultValue: 0,
    componentProps: {
      checkedValue: 1,
      unCheckedValue: 0,
      checkedChildren: '是',
      unCheckedChildren: '否',
    },
    ifShow: ({ values }) => isMenu(values.ismenu),
  },
  {
    field: 'show',
    label: '是否显示',
    component: 'Switch',
    defaultValue: 1,
    componentProps: {
      checkedValue: 1,
      unCheckedValue: 0,
      checkedChildren: '是',
      unCheckedChildren: '否',
    },
    ifShow: ({ values }) => isMenu(values.ismenu),
  },
  {
    field: 'remark',
    label: '备注',
    component: 'InputTextArea',
  },
  {
    field: 'weigh',
    label: '排序',
    component: 'InputNumber',
    defaultValue: 0,
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '禁用', value: 0 },
      ],
    },
  },
];
