import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { uploadApi } from '/@/api/sys/upload';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 300,
    defaultHidden: true,
  },
  {
    title: '文件名称',
    dataIndex: 'filename',
    width: 300,
  },
  {
    title: 'sha1',
    dataIndex: 'sha1',
  },
  {
    title: '文件大小',
    dataIndex: 'filesize',
    width: 100,
  },
  {
    title: '文件后缀',
    dataIndex: 'suffix',
    width: 100,
  },
  {
    title: '文件类型',
    dataIndex: 'mimetype',
    width: 100,
  },
  {
    title: '存储类型',
    dataIndex: 'storage',
    width: 100,
    customRender: ({ record }) => {
      const storage = record.storage;
      const color = {
        local: 'success',
        aliyun: 'processing',
        qiniu: 'error',
        qcloud: 'warning',
      };
      const text = {
        local: '本地',
        aliyun: '阿里云',
        qiniu: '七牛云',
        qcloud: '腾讯云',
      };

      return h(Tag, { color: color[storage] }, () => text[storage]);
    },
  },
  {
    title: '上传时间',
    dataIndex: 'createtime',
    width: 200,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'filename',
    label: '文件名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'time',
    label: '发布时间',
    component: 'RangePicker',
    colProps: { span: 6 },
  },
  {
    field: 'suffix',
    label: '文件后缀',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'storage',
    label: '存储类型',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '本地',
          value: 'local',
        },
        {
          label: '阿里云',
          value: 'aliyun',
        },
        {
          label: '七牛云',
          value: 'qiniu',
        },
        {
          label: '腾讯云',
          value: 'qcloud',
        },
      ],
    },
    colProps: { span: 6 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'storage',
    label: '存储类型',
    component: 'Select',
    defaultValue: 'local',
    componentProps: {
      options: [
        {
          label: '本地',
          value: 'local',
        },
        {
          label: '阿里云',
          value: 'aliyun',
        },
        {
          label: '七牛云',
          value: 'qiniu',
        },
        {
          label: '腾讯云',
          value: 'qcloud',
        },
      ],
    },
    colProps: { span: 12 },
  },
  {
    label: '文件',
    field: 'avatar',
    component: 'Upload',
    componentProps: ({ formModel, formActionType }) => {
      return {
        api: uploadApi,
        maxSize: 5,
        maxNumber: 3,
        emptyHidePreview: true,
        accept: ['image/*', 'doc', 'docx', 'xls', 'xlsx', 'pdf'],
        uploadParams: { type: formModel.storage },
      };
    },
  },
];
