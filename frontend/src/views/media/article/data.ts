import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '所属分组',
    dataIndex: 'module.group.name',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '所属网站',
    dataIndex: 'module.website.name',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '模块名称',
    dataIndex: 'module.name',
    width: 200,
    slots: { customRender: 'module' },
  },
  {
    title: '文章标题',
    dataIndex: 'title',
    //width: 300,
    slots: { customRender: 'link' },
    //align: 'left',
  },
  {
    title: '封面',
    dataIndex: 'cover',
    width: 150,
    slots: { customRender: 'image' },
    defaultHidden: true,
  },
  {
    title: '文章地址',
    dataIndex: 'link',
    defaultHidden: true,
  },
  {
    title: '发布时间',
    dataIndex: 'time',
    width: 200,
  },
  {
    title: '采集时间',
    dataIndex: 'createtime',
    width: 200,
    defaultHidden: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'title',
    label: '文章标题',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'time',
    label: '发布时间',
    component: 'RangePicker',
    colProps: { span: 6 },
  },
];
