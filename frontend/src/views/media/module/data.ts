import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { AllGroupList, AllWebsiteList } from '/@/api/demo/media';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '所属分组',
    dataIndex: 'group.name',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '所属网站',
    dataIndex: 'website.name',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '模块名称',
    dataIndex: 'name',
    width: 200,
    slots: { customRender: 'module' },
  },
  {
    title: '链接',
    dataIndex: 'url',
    slots: { customRender: 'link' },
  },
  {
    title: '文章数',
    dataIndex: 'article_num',
    width: 100,
  },
  {
    title: '采集频率',
    dataIndex: 'times',
    width: 100,
    customRender: ({ record }) => {
      const times = record.times;
      const color = {
        60: 'default',
        300: 'success',
        600: 'processing',
        1800: 'warning',
        3600: 'error',
      };
      const text = {
        60: '1分钟',
        300: '5分钟',
        600: '10分钟',
        1800: '30分钟',
        3600: '1小时',
      };

      return h(Tag, { color: color[times] }, () => text[times]);
    },
  },
  {
    title: '错误次数',
    dataIndex: 'error_count',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '错误信息',
    dataIndex: 'error_msg',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '排序',
    dataIndex: 'weigh',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createtime',
    width: 200,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'group_id',
    label: '所属分组',
    component: 'ApiSelect',
    componentProps: ({ formModel, formActionType }) => {
      return {
        api: AllGroupList,
        labelField: 'name',
        valueField: 'id',
        onChange: (e) => {
          console.log('selected:', e);
          const { updateSchema } = formActionType;
          //重置变量
          formModel.website_id = undefined;
          AllWebsiteList({ group_id: e }).then((res) => {
            const list = res;
            const websiteList = [];
            for (let i = 0; i < list.length; i++) {
              websiteList[i] = {
                label: list[i]['name'],
                value: list[i]['id'],
                key: list[i]['id'],
              };
            }
            console.log(list);
            console.log(websiteList);
            //赋值
            updateSchema({
              field: 'website_id',
              componentProps: {
                options: websiteList,
              },
              colProps: { span: 6 },
            });
          });
        },
        // atfer request callback
        onOptionsChange: (options) => {
          console.log('get options', options.length, options);
        },
      };
    },
    colProps: { span: 6 },
  },
  {
    field: 'website_id',
    label: '所属网站',
    component: 'Select',
    componentProps: {
      options: [],
      placeholder: '请选择网站',
    },
    colProps: { span: 6 },
  },
  {
    field: 'name',
    label: '模块名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'url',
    label: '链接',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '启用',
          value: 1,
        },
        {
          label: '停用',
          value: 0,
        },
      ],
    },
    colProps: { span: 6 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'InputNumber',
    defaultValue: 0,
    show: false,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
  {
    field: 'group_id',
    label: '所属分组',
    component: 'ApiSelect',
    componentProps: ({ formModel, formActionType }) => {
      return {
        api: AllGroupList,
        labelField: 'name',
        valueField: 'id',
        onChange: (e) => {
          console.log('selected:', e);
          const { updateSchema } = formActionType;
          //重置变量
          formModel.website_id = undefined;
          AllWebsiteList({ group_id: e }).then((res) => {
            const list = res;
            const websiteList = [];
            for (let i = 0; i < list.length; i++) {
              websiteList[i] = {
                label: list[i]['name'],
                value: list[i]['id'],
                key: list[i]['id'],
              };
            }
            console.log(list);
            console.log(websiteList);
            //赋值
            updateSchema({
              field: 'website_id',
              componentProps: {
                options: websiteList,
              },
            });
          });
        },
        // atfer request callback
        onOptionsChange: (options) => {
          console.log('get options', options.length, options);
        },
      };
    },
  },
  // {
  //   field: 'website_id',
  //   label: '所属网站',
  //   required: true,
  //   component: 'ApiSelect',
  //   componentProps: {
  //     api: AllWebsiteList,
  //     labelField: 'name',
  //     valueField: 'id',
  //   },
  // },
  {
    field: 'website_id',
    label: '所属网站',
    required: true,
    component: 'Select',
    componentProps: {
      options: [],
      placeholder: '请选择网站',
    },
  },
  {
    field: 'name',
    label: '模块名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'url',
    label: '链接',
    required: true,
    component: 'Input',
  },
  {
    field: 'intro',
    label: '简介',
    component: 'InputTextArea',
    componentProps: {
      rows: 3,
    },
  },
  {
    field: 'times',
    label: '采集频率',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '1分钟',
          value: 60,
        },
        {
          label: '5分钟',
          value: 300,
        },
        {
          label: '10分钟',
          value: 600,
        },
        {
          label: '30分钟',
          value: 1800,
        },
        {
          label: '1小时',
          value: 3600,
        },
      ],
    },
    defaultValue: 600,
  },
  {
    field: 'rules',
    label: '采集规则',
    required: true,
    component: 'InputTextArea',
    componentProps: {
      rows: 5,
    },
    slot: 'json',
  },
  {
    field: 'weigh',
    label: '排序',
    component: 'InputNumber',
    defaultValue: 0,
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '停用', value: 0 },
      ],
    },
  },
];
