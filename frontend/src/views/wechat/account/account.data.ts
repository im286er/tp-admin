import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { uploadApi } from '/@/api/sys/upload';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '头像',
    dataIndex: 'head_img',
    width: 100,
    slots: { customRender: 'head_img' },
  },
  {
    title: '公众号名称',
    dataIndex: 'nickname',
    width: 150,
    slots: { customRender: 'link' },
  },
  {
    title: '微信号',
    dataIndex: 'alias',
    // width: 150,
  },
  {
    title: 'APPID',
    dataIndex: 'appid',
    // width: 150,
  },
  {
    title: 'FAKEID',
    dataIndex: 'fakeid',
    // width: 150,
    defaultHidden: true,
  },
  // {
  //   title: 'APPID',
  //   dataIndex: 'appid',
  //   width: 200,
  // },
  {
    title: '描述',
    dataIndex: 'signature',
    defaultHidden: true,
  },
  {
    title: '二维码',
    dataIndex: 'qrcode',
    width: 100,
    slots: { customRender: 'qrcode' },
  },
  {
    title: '文章数量',
    dataIndex: 'msg_count',
    width: 100,
  },
  {
    title: '公众号类型',
    dataIndex: 'service_type',
    width: 100,
    customRender: ({ record }) => {
      const type = record.service_type;
      const enable = ~~type === 2;
      const color = enable ? 'warning' : 'processing';
      const text = enable ? '服务号' : '订阅号';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '排序',
    dataIndex: 'weigh',
    width: 50,
    defaultHidden: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createtime',
    width: 200,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'nickname',
    label: '公众号名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'alias',
    label: '微信号',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'InputNumber',
    defaultValue: 0,
    show: false,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
  {
    field: 'keyword',
    label: '搜索',
    component: 'Input',
    slot: 'remoteSearch',
    ifShow: ({ values }) => {
      return values.id === 0;
    },
    //required: true,
  },
  {
    field: 'wechat',
    label: '公众号',
    component: 'Select',
    ifShow: ({ values }) => {
      return values.id === 0;
    },
    required: true,
  },
  {
    field: 'nickname',
    label: '公众号',
    component: 'Input',
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
    required: true,
    dynamicDisabled: true,
  },
  {
    field: 'alias',
    label: '微信号',
    component: 'Input',
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
    required: true,
    dynamicDisabled: true,
  },
  {
    label: '头像',
    field: 'head_img',
    component: 'Upload',
    componentProps: {
      api: uploadApi,
      maxSize: 5,
      maxNumber: 1,
      emptyHidePreview: true,
      //accept: ['image/*'],
    },
    ifShow: false,
  },
  {
    field: 'weigh',
    label: '排序',
    component: 'InputNumber',
    defaultValue: 0,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
    required: true,
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '禁用', value: 0 },
      ],
    },
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
];
