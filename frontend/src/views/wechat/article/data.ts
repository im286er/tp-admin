import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '公众号名称',
    dataIndex: 'a_nickname',
    width: 150,
    slots: { customRender: 'account' },
  },
  {
    title: '文章标题',
    dataIndex: 'title',
    //width: 300,
    slots: { customRender: 'link' },
    //align: 'left',
  },
  {
    title: '封面',
    dataIndex: 'cover',
    width: 150,
    slots: { customRender: 'image' },
  },
  {
    title: '显示位置',
    dataIndex: 'itemidx',
    width: 100,
  },
  {
    title: '作者',
    dataIndex: 'author',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '摘要',
    dataIndex: 'digest',
    defaultHidden: true,
  },
  {
    title: '文章地址',
    dataIndex: 'link',
    defaultHidden: true,
  },
  {
    title: '文章类型',
    dataIndex: 'copyright_type',
    width: 100,
    customRender: ({ record }) => {
      const type = record.copyright_type;
      const enable = ~~type === 1;
      const color = enable ? 'warning' : 'processing';
      const text = enable ? '原创' : '其他';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '写作时间',
    dataIndex: 'createtime',
    width: 200,
    defaultHidden: true,
  },
  {
    title: '发布时间',
    dataIndex: 'updatetime',
    width: 200,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'title',
    label: '文章标题',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'author',
    label: '作者',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'copyright_type',
    label: '文章类型',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '原创',
          value: 1,
        },
        {
          label: '其他',
          value: 0,
        },
      ],
    },
    colProps: { span: 6 },
  },
];

export const searchFormSchemaIndex: FormSchema[] = [
  {
    field: 'nickname',
    label: '公众号名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'title',
    label: '文章标题',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'time',
    label: '发布时间',
    component: 'RangePicker',
    colProps: { span: 6 },
  },
  {
    field: 'copyright_type',
    label: '文章类型',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '原创',
          value: 1,
        },
        {
          label: '其他',
          value: 0,
        },
      ],
    },
    colProps: { span: 6 },
  },
];
